# 书签整理

## 有趣的项目

### 机器学习

1. [MLflow|Python|全生命周期的机器学习平台](https://github.com/mlflow/mlflow)
2. [FaceNet|Python|人脸识别 FaceNet，有点老了](https://github.com/davidsandberg/facenet)
3. [Iris_Osiris|c++|虹膜识别](https://github.com/5455945/Iris_Osiris)
   - [虹膜识别 Iris_Osiris_v4.1|site](https://blog.csdn.net/longji/article/details/78298582)
4. [chineseocr_lite|c++|轻量的中文 orc 库](https://github.com/DayBreak-u/chineseocr_lite)
5. [bolt|c++|超快的矩阵和向量运算库](https://github.com/dblalock/bolt)
6. [MockingBird|python|支持中文的实时语音克隆](https://github.com/babysor/MockingBird)
7. [PaddleOCR|python|百度推出的 ocr 识别库](https://github.com/PaddlePaddle/PaddleOCR)
8. [gorse|go|golang 实现的开源推荐系统](https://github.com/zhenghaoz/gorse)
9. [yolov5|python|图片对象识别](https://github.com/ultralytics/yolov5)
10. [RobustVideoMatting|python|字节跳动开发的超强视频抠图库](https://github.com/PeterL1n/RobustVideoMatting)
11. [mediapipe|c++|直播流中的人工智能工具箱，功能比较全](https://github.com/google/mediapipe)

---

### 服务端架构

1. [openfaas|go|serveless|开源 faas 平台](https://github.com/openfaas)
   - [openfaas 不支持挂载卷|site](https://github.com/openfaas/faas/issues/320)
   - [OpenFaaS 实战|site|openfaas 博客系列文章教学](https://www.imooc.com/article/317573)
   - [openfaas 超时配置|site|需要改好几个地方](https://github.com/openfaas/faasd/issues/69)
2. [fission|go|serveless|openfaas 的竞品](https://github.com/fission/fission)
3. [OpenWhisk|Scala|apache 开源的 faas 平台，应用也比较广](https://github.com/apache/openwhisk)
4. [CNI|go|The Container Network Interface|基础组件，用于管理容器网络](https://github.com/containernetworking/cni)
5. [runc|go|runc is a CLI tool for spawning and running containers on Linux according to the OCI specification|为 containerd 提供支撑，控制容器运行](https://github.com/opencontainers/runc)
6. [containerd|go|容器运行时，没有 docker 那么臃肿，自己写 go 程序可实现容器全生命周期管理](https://github.com/containerd/containerd)
   - [使用 CNI 为 Containerd 容器添加网络能力|site](https://blog.frognew.com/2021/04/relearning-container-03.html)
   - [gVisor 关于 CNI 的教程|site](https://gvisor.dev/docs/tutorials/cni/)
   - [issues|site|Unable to pull DockerHub "library" images](https://github.com/containerd/containerd/issues/3236)
7. [nerdctl|go|containerd 的命令行工具，支持 compose-file](https://github.com/containerd/nerdctl)
8. [Linkerd|go|service mesh|服务网格](https://github.com/linkerd/linkerd2)
9. [Istio|go|微服务管理框架](https://github.com/istio/istio)
10. [EaseGress|go|国产流量编排系统，号称不改一行代码做秒杀](https://github.com/megaease/easegress)
11. [Terraform|go|IT 基础架构自动化编排工具，解决的就是在云上那些硬件资源分配管理的问题](https://github.com/hashicorp/terraform)
12. [Sogou C++ Workflow|c++|搜狗公司 C++服务器引擎，支撑搜狗几乎所有后端 C++在线服务](https://github.com/sogou/workflow)
13. [Serverless|javascript|开源 serverless sdk](https://github.com/serverless/serverless)

---

### 服务端应用

1. [Chaosblade|go|阿里巴巴开源的一款简单易用、功能强大的混沌实验注入工具](https://github.com/chaosblade-io/chaosblade)
2. [JumpServer|python|开源堡垒机，管理内部资产](https://github.com/jumpserver/jumpserver)
3. [DTM|golang|支持多语言接入的分布式事务管理器，可以一次回滚整个分布式上的事务](https://github.com/dtm-labs/dtm)

---

### 数据库

1. [PostgREST|haskell|通过 RESTful Api 访问 PostgreSQL，依托数据库服务器的算力](https://github.com/PostgREST/postgrest)
2. [etcd|go|分布式键值数据库，非常可靠，适合做配置中心](https://github.com/etcd-io/etcd)
3. [gorm|go|golang 的 ORM 框架](https://github.com/go-gorm/gorm)
4. [minio|go|分布式对象存储，性能好，支持数据损坏还原](https://github.com/minio/minio)
5. [ClickHouse|c++|列存储数据库，适用于大数据分析](https://github.com/ClickHouse/ClickHouse)
6. [NocoDB|nodejs|web 直连各种数据库，可视化表格流程界面操作](https://github.com/nocodb/nocodb)
7. [TiDB|go|国产开源分布式数据库集群，兼容 MySQL 协议，推荐](https://github.com/pingcap/tidb)
8. [ceph|c++|分布式对象存储，性能好，支持动态扩容，运维复杂](https://github.com/ceph/ceph)
9. [influxdb|go|开源时序数据库](https://github.com/influxdata/influxdb)
10. [immudb|go|防篡改数据库，可以看到数据的所有历史记录，有版本管理的功能](https://github.com/codenotary/immudb)
11. [MMKV|c++|腾讯应用在微信中的高性能 kv 数据库，通过共享内存可实现多进程通信](https://github.com/Tencent/MMKV)
12. [cockroach|go|分布式数据库，根据谷歌的论文实现，推荐](https://github.com/cockroachdb/cockroach)

---

### 网络应用

1. [go-imap|go|电子邮件|imap 库](https://github.com/emersion/go-imap)
2. [rclone|go|用于在多个网络存储供应商上共享和同步文件的命令行程序](https://github.com/rclone/rclone)
3. [frp|go|用于内网穿透的高性能的反向代理应用](https://github.com/fatedier/frp)
4. [nps|go|功能强大的内网穿透代理服务器](https://github.com/ehang-io/nps)
5. [Authelia|go|SSO|开源身份验证和授权服务器](https://github.com/authelia/authelia)
6. [gun|javascript|web 上的 p2p 数据传输库](https://github.com/amark/gun)
7. [filebrowser|golang|go+vue 实现的超棒的 web 文件管理器](https://github.com/filebrowser/filebrowser)
8. [gitea|go|golang 实现的自托管 git 服务器](https://github.com/go-gitea/gitea)
9. [flat|nodejs|一款开源在线教学系统](https://github.com/netless-io/flat)
10. [OpenTelemetry|go|编程语言指标监控监测](https://github.com/open-telemetry)
11. [protobuf|c++|google 推出的 protobuf](https://github.com/protocolbuffers/protobuf)
12. [go-zero|go|好未来推出的 rpc 网络框架，提供一键生成多端代码的功能](https://github.com/zeromicro/go-zero)
13. [LiveKit|go|golang 实现的分布式 webrtc 视频音频会议系统](https://github.com/livekit/livekit-server)
14. [substrate|rust|下一代区块链技术框架](https://github.com/paritytech/substrate)
15. [Iris|go|性能不错的网络框架，可以与 gin 对比一下](https://github.com/kataras/iris)
16. [traefik|go|网关应用](http://traefik.io)

- [中文文档|site](https://www.qikqiak.com/traefik-book/)

17. [Open-IM-Server|go|开源 im，从服务端到客户端都有了](https://github.com/OpenIMSDK/Open-IM-Server)

---

### 前端项目

1. [fontmin|nodejs|字体剪裁，超棒](https://github.com/ecomfe/fontmin)
2. [Blockly|nodejs|Google 推出的可视化编程工具](https://github.com/google/blockly)
3. [MathQuill|nodejs|超酷的在线公式编辑器](https://github.com/mathquill/mathquill)
4. [D3: Data-Driven Documents|javascript|强大的前端数据驱动的图表显示组件](https://github.com/d3/d3)
5. [Fabric.js|javascript|canvans 与 svg 互相转换](https://github.com/fabricjs/fabric.js)
6. [ffmpeg.wasm|javascript|web 端的 ffmpeg](https://github.com/ffmpegwasm/ffmpeg.wasm)
7. [zigbee2mqtt|javascript|可以不用网关让 zigbee 设备接入 mqtt](https://github.com/Koenkk/zigbee2mqtt)
8. [Vue.Draggable|javascript|vue 拖拽能力](https://github.com/SortableJS/Vue.Draggable)
9. [GrapesJS|javascript|在线设计 web 页面](https://github.com/artf/grapesjs)
10. [SheetJS|nodejs|Excel 解析库](https://github.com/SheetJS/sheetjs)
11. [envoy|c++|类似 traefik 的网络代理工具](https://github.com/envoyproxy/envoy)
12. [Envoy 中文指南|site](https://fuckcloudnative.io/envoy-handbook/)
13. [vue-form-making|javascript|基于 vue 设计的可视化表单设计工具](https://github.com/GavinZhuLei/vue-form-making)
14. [superset|javascript|apache 开源的一款可以对多种数据源进行数据可视化和探索的工具，无需代码编程即可生成漂亮的可视化图表](https://github.com/apache/superset)
15. [百度开源 amis|javascript|前端低代码框架，通过 JSON 配置就能生成各种后台页面，极大减少开发成本，甚至可以不需要了解前端](https://gitee.com/baidu/amis)
16. [wangEditor|javascript|轻量富文本编辑工具](https://github.com/wangeditor-team/wangEditor)
17. [Tencent-APIJSON|javascript|低代码开发，前端按需请求接口，后端自动生成，适合中小型项目](https://github.com/Tencent/APIJSON)

---

### 图形处理

1. [gui-lite|c++|IOT|4000 行头文件 gui 库](https://gitee.com/idea4good/GuiLite)
2. [Notcurses|c|命令行界面的图形库](https://github.com/dankamongmen/notcurses)
3. [ImGui|c++|无状态的 gui 绘制库](https://github.com/ocornut/imgui)
4. [Theatrejs|nodejs|一款在线动画编辑工具，效果很棒](https://github.com/AriaMinaei/theatre)
5. [Kornia|Python|基于 PyTorch 的可微分的计算机视觉库](https://github.com/kornia/kornia)
6. [iced|rust|rust 可用的跨平台 gui 库](https://github.com/iced-rs/iced)
7. [tauri|rust|rust 的跨平台 ui 库](https://github.com/tauri-apps/tauri)
8. [Manim|python|超强的数学动画库](https://github.com/ManimCommunity/manim)
   - [3b1b/manim|python|同上，个人分支，产出了大量的数学动画](https://github.com/3b1b/manim)

---

### 3D

1. [Endless Sky|c++|开源的太空游戏](https://github.com/endless-sky/endless-sky)
2. [Open3D|c++|3d 数据处理库](https://github.com/isl-org/Open3D)
3. [FlaxEngine|c++|3d 游戏引擎](https://github.com/FlaxEngine/FlaxEngine)
4. [Google-Filament|c++|Filament is a real-time physically based rendering engine](https://github.com/google/filament)

### 娱乐

1. [RPCS3|c++|ps3 模拟器](https://github.com/RPCS3/rpcs3)
2. [yuzu|c++|任天堂 ns 模拟器](https://github.com/yuzu-emu/yuzu)

---

### 能力组件

1. [xresloader|java|跨平台 Excel 导表工具，功能比较全](https://github.com/xresloader/xresloader)
2. [Excelize|go|golang 处理微软 excel 表格](https://github.com/qax-os/excelize)
3. [unioffice|go|golang 处理微软文档](https://github.com/unidoc/unioffice)
4. [cobra|go|超棒的命令行库](https://github.com/spf13/cobra)
5. [Viper|go|多种配置文件读取工具，可以从 etcd 中读取，目前正在演化到 v2 版本，持续关注](https://github.com/spf13/viper)
6. [fzf|go|极快的命令行模糊搜素工具](https://github.com/junegunn/fzf)
7. [Vault|go|各种安全密钥存储、生成工具平台](https://github.com/hashicorp/vault)

---

### 小工具

1. [slides|go|可以将 markdown 文件显示在终端翻页显示，可以执行内置的代码](https://github.com/maaslalani/slides)

---

### 嵌入式

1. [RT-Thread|c|RTOS|国产实时操作系统，适应广泛](https://github.com/RT-Thread)

---

### 编程语言

1. [project-layout|go|golang 工程布局推荐](https://github.com/golang-standards/project-layout)
2. [go-admin|go|golang 的中台框架](https://github.com/GoAdminGroup/go-admin)
3. [go-admin|go|另一个 golang 的中台框架](https://github.com/go-admin-team/go-admin)
4. [gin-vue-admin|go|看起来非常不错的 golang 中台框架](https://github.com/flipped-aurora/gin-vue-admin)
5. [awesome-go|go|golang 优秀项目集合](https://github.com/avelino/awesome-go)
6. [gin|go|golang 优秀的 http 框架](https://github.com/gin-gonic/gin)
7. [go-zero|go|golang 优秀的 rpc 框架](https://gitee.com/kevwan/go-zero)
8. [gf|go|golang 基础网络框架](https://github.com/gogf/gf)
9. [mobile|go|在移动平台中使用 golang](https://github.com/golang/mobile)
10. [DLang|d|d 语言，据说有很多优秀的特性](https://github.com/dlang)
11. [guava|java|google 积累的一套基础库](https://github.com/google/guava)

---

### 实用工具

1. [WinMerge|c++|文件对比合并工具，超好用](https://github.com/WinMerge/winmerge)
2. [btop|c++|超酷的系统监控界面，支持在命令行下显示，暂不支持 windows 系统](https://github.com/aristocratos/btop)
3. [hugo|golang|一键生成静态 html 和 css 的工具，广泛用于网站和博客](https://github.com/gohugoio/hugo)
4. [syncthing|golang|文件自动同步工具](https://github.com/syncthing/syncthing)
5. [docker-ipsec-vpn-server|shell|一键搭建 ipsec vpn 服务器搭建脚本](https://github.com/hwdsl2/docker-ipsec-vpn-server)
6. [DolphinScheduler|java|apache 开源的任务调度编排工具，可视化编辑](https://github.com/apache/dolphinscheduler)
7. [kubescape|go|用于分析 Kubernetes 安全性的工具](https://github.com/armosec/kubescape)
8. [modern-unix|shell|收集了一些优秀的命令行工具](https://github.com/ibraheemdev/modern-unix)
9. [asme.sh|shell|自动更新证书](https://github.com/acmesh-official/acme.sh)

---

## 有趣的网站

### 学习类

1. [FreeCodeCamp|site|免费在线学习编程，有证书，节奏可控，完成可入大厂](https://www.freecodecamp.org/)
2. [TheAlgorithms|site|涵盖多种编程语言实现的算法示例](https://github.com/TheAlgorithms)
3. [互联网 Java 工程师进阶知识完全扫盲|site|有很多设计上的经验分享，非常值得学习，涵盖高并发、分布式、高可用、微服务、海量数据处理的方方面面](https://github.com/doocs/advanced-java)
4. [互联网公司常用框架源码赏析|site](https://github.com/doocs/source-code-hunter)
5. [CP-Algorithms|site|一些经典问题的算法，推荐](https://cp-algorithms.web.app/)
6. [Flutter 每周小部件教学|site](https://www.youtube.com/watch?v=b_sQ9bMltGU&list=PLjxrf2q8roU23XGwz3Km7sQZFTdB996iG)
7. [系统架构设计师复习资料|site](https://github.com/xxlllq/system_architect)
8. [1earn|site|安全知识全面收集整理](https://github.com/ffffffff0x/1earn)
9. [代码安全指南|site|涵盖多种语言的安全编码规范](https://github.com/Tencent/secguide)
10. [hello-algorithm|site|收集了大量的算法图解，还有大量的思维导图](https://github.com/geekxh/hello-algorithm)
11. [awesome-flutter|dart|flutter 里面好用的库收集](https://github.com/Solido/awesome-flutter)
12. [awesome-selfhosted|javascript|大量的项目推荐](https://github.com/awesome-selfhosted/awesome-selfhosted)
13. [GitHub-Chinese-Top-Charts|site|汇聚各种语言的中文项目推荐](https://github.com/kon9chunkit/GitHub-Chinese-Top-Charts)
14. [前端精读|site](https://github.com/ascoders/weekly)
15. [applied-ml|汇集了大量的机器学习论文和知识点介绍，背后原理等](https://github.com/eugeneyan/applied-ml)
16. [Linux 云计算网络|site|汇集了大量的网络虚拟化技术、容器技术基础概念的讲解，推荐](https://www.cnblogs.com/bakari/p/10529575.html)
17. [hacker-laws-zh|site|hacker-laws 的的中文翻译，对开发人员有用的定律、理论、原则和模式](https://github.com/nusr/hacker-laws-zh)
18. [后端架构师知识图谱|site|java 方向的，文章类，在线可直接看，方便](https://github.com/xingshaocheng/architect-awesome)
19. [互联网公司经典技术架构|site|技术架构及开源项目](https://github.com/davideuler/architecture.of.internet-product)

---

### 软件工具

1. [KiCad|site|免费的 cad 软件]
2. [JThatch.com|site|有一些有趣的在线工具，导出地球地形，月球地形等](https://jthatch.com/)
3. [clipconverter.cc|site|下载 youtube 视频](https://www.clipconverter.cc/2/)
4. [Supernova|site|管理设计资源，同步到代码](https://supernova.io/)
5. [Cloudflare|site|提供 DNS 代理，HTTPS 等服务](https://www.cloudflare.com/zh-cn/)
6. [水泽-信息收集自动化工具|site|网站安全扫描](https://github.com/0x727/ShuiZe_0x727)
7. [Ventoy|site|制作多种镜像的 usb 启动器](https://github.com/ventoy/Ventoy)

---

### 资源

1. [free-font|site|免费可商用字体收集](https://github.com/wordshub/free-font)
2. [public-apis|site|收集了很多公开的 api](https://github.com/public-apis/public-apis)
3. [google-research|site|google 开源的最新研究成果](https://github.com/google-research/google-research)
   - [官网|site](https://research.google/)
4. [国家中小学网络平台|site|教学大纲网课资源](https://ykt.eduyun.cn/ykt/sjykt/index.html?from=timeline&isappinstalled=0)
5. [霞鹜文楷|site|免费商用字体](https://github.com/lxgw/LxgwWenKai)
6. [Pixabay|site|免费正版高清图片素材库](https://pixabay.com/)
7. [sci-hub|site|论文搜索下载](https://sci-hubtw.hkvisa.net/)
8. [大木虫学术导航|site|论文学术搜索等 口令：2020](http://4243.net/)
9. [图精灵|site|下载图片素材，已买终生会员](http://616pic.com/)
10. [中文优秀项目推荐|site|中文优秀项目推荐，每周更新](https://github.com/kon9chunkit/GitHub-Chinese-Top-Charts#All-Language)

---

## 知识点

### 3D

1. [USD|site|通用场景描述](https://graphics.pixar.com/usd/docs/index.html)
2. [知乎 x-tesla|site|有一些 UE4 开发相关的博客文章](https://www.zhihu.com/people/x-tesla/posts)

---

### 编程开发

1. [交叉编译器的命名规则及详细解释|site](https://blog.csdn.net/LEON1741/article/details/81537529)

---

### 硬件

1. [KTechLab|site|电子元件硬件设计仿真工具](https://github.com/ktechlab/ktechlab)
2. [CircuitJS1|site|电路模拟器](https://github.com/sharpie7/circuitjs1)
3. [NFC CPU 卡在实际产品中的操作流程及原理|site](https://blog.csdn.net/liwei16611/article/details/85164625)
4. [浅谈 M1 卡（水卡、饭卡、开水卡和门禁卡等）的破解与复制——理论知识篇|site](https://www.bilibili.com/read/cv6476113?from=articleDetail)
5. [非接触 CPU 卡芯片|site](http://www.fmsh.com/38bd06c9-d430-70e2-eb5c-393c198068a4/)

---

### Linux

1. [mount 挂载相关|site|bind|rbind](http://www.wutianqi.com/blog/3699.html)
   - [Create a Loop in a Linux filesystem|site|循环依赖挂载](https://stackoverflow.com/questions/730589/create-a-loop-in-a-linux-filesystem)
2. [蜗窝科技|site|有很多 linux 开发技术，很硬核](http://www.wowotech.net/)

---

### 运维

1. [一文搞懂蓝绿部署和金丝雀发布|site|里面有 CI/CD 系列教程文章](https://www.cnblogs.com/rancherlabs/p/12763359.html)
2. [深入了解 CI/CD：工具、方法、环境、基础架构的全面指南|site](https://mp.weixin.qq.com/s/ljfZvitCyGtpNwBZg67vhQ)
3. [Docker 部署 Mysql 集群|site|mysql 集群搭建的方案](https://www.cnblogs.com/wanglei957/p/11819547.html)
4. [Kubernetes 知识图谱|site|知识点涵盖的比较全面](https://www.processon.com/view/link/5ac64532e4b00dc8a02f05eb#map)
5. [完整优雅的卸载腾讯云云服务器安全监控组件|site](https://cloud.tencent.com/developer/article/1381482)

---

### 操作系统

1. [the-art-of-command-line|site|操作系统命令行技巧总结](https://github.com/jlevy/the-art-of-command-line/blob/master/README-zh.md)

---

### 容器技术

1. [Open Container Initiative Runtime Specification|site|OCI 运行时标准](https://github.com/opencontainers/runtime-spec)
   - [config.md|site|配置选项介绍和示例](https://github.com/opencontainers/runtime-spec/blob/master/config.md)
2. [Get storage for your Severless Functions with Minio & Docker|site|openfaas|openfaas 不支持挂载卷，使用对象存储](https://blog.alexellis.io/openfaas-storage-for-your-functions/)

---

### 文档

1. [软件工程之软件概要设计|site](https://www.cnblogs.com/youcong/p/9500921.html)
2. [软件工程的形式化方法第六讲|site](https://www.docin.com/p-485138963.html)
3. [软件项目实施标准方案模板（完整版）|site](https://max.book118.com/html/2018/0528/169119380.shtm)
4. [bulletjournal|site|子弹笔记教程](https://bulletjournal.com/pages/learn)

---

## 娱乐

### 电影

1. [片吧影院|site|比较全，能在线看](https://www.pianba.net/)

---

### 软件

1. [MTMR|site|macbook touchbar 定制小工具](https://github.com/Toxblh/MTMR)

---

### 魔方

1. [魔方的高级玩法(CFOP 方法)|site](http://www.rubik.com.cn/fridrich091.htm)
2. [i-mofang|site](http://www.i-mofang.com/bb_21.shtml#maintop)

---

### 待整理

1. [TisScript|dart|flutter 开发的可视化节点编程](https://github.com/saurabhguptarock/TisScript)
2. [youlai-mall|java|一套全栈开源商城项目](https://github.com/hxrui/youlai-mall)
3. [GANSketching|python|根据手绘线稿生成猫咪图片](https://github.com/PeterWang512/GANSketching)
4. [draw|javascript|web 端支持压感的手写输入墨迹](https://github.com/amoshydra/draw)
5. [ferry|go|国产开源工单系统](https://github.com/lanyulei/ferry)
